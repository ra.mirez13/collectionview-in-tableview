//
//  IphoneModel.swift
//  test
//
//  Created by Игорь on 17/12/2018.
//  Copyright © 2018 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct IphoneModel : Codable {
    let title : String?
    let images : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case title = "title"
        case images = "images"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        images = try values.decodeIfPresent([String].self, forKey: .images)
    }
}

struct ResponseList : Codable {
    let list : [IphoneModel]?
    
    enum CodingKeys: String, CodingKey {
        case list
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([IphoneModel].self, forKey: .list)
    }
}


