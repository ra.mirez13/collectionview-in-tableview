//
//  subClass.swift
//  test
//
//  Created by Konstantin Chukhas on 12/17/18.
//  Copyright © 2018 Konstantin Chukhas. All rights reserved.
//

import UIKit
import SDWebImage
import AlamofireImage

class subClass: UITableViewCell {

    var iphones:IphoneModel?
    
    var apiManager:APIManager = APIManager()
    
    @IBOutlet weak var collectionView: UICollectionView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    func setImagesToCollection (cData:IphoneModel) {
        iphones = cData
        collectionView.reloadData()
    }
    

    
}

extension subClass: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = iphones?.images?.count ?? 0
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IphonesCell", for: indexPath) as! IphonesCollectionViewCell
        if let iphone = iphones {
            if let imgUrl = iphone.images?[indexPath.row] {
                cell.img.sd_setImage(with: URL(string: imgUrl), placeholderImage: nil)
            }
        }
        
       
        return cell
    }
    
    
    
    
}
