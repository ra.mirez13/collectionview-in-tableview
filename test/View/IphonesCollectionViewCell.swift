//
//  IphonesCollectionViewCell.swift
//  test
//
//  Created by Konstantin Chukhas on 12/17/18.
//  Copyright © 2018 Konstantin Chukhas. All rights reserved.
//

import UIKit

class IphonesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    
    var myIphone:Iphones!{
        didSet{
            updateData()
        }
    }
func updateData(){
        img.image = myIphone.images
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
        self.clipsToBounds = true 
    }
}
