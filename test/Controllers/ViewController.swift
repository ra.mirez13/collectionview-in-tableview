//
//  ViewController.swift
//  test
//
//  Created by Konstantin Chukhas on 12/17/18.
//  Copyright © 2018 Konstantin Chukhas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //MARK:Variables
    var apiManager:APIManager = APIManager()
    var categories  = ["Iphone7","Iphone x"]
    var list = [IphoneModel]()
      //MARK:Outlets
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager.login { (result) in
            if let responseList = result {
                self.list = responseList
                self.tableView.reloadData()
            }
        }
      
    }
}
  //MARK:UITableViewDelegats
extension ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String?{
        return list[section].title ?? ""
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! subClass
        cell.setImagesToCollection(cData: list[indexPath.section])
       return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 169
    }
   }
