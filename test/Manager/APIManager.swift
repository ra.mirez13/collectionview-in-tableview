//
//  APIManager.swift
//  test
//
//  Created by Konstantin Chukhas on 12/17/18.
//  Copyright © 2018 Konstantin Chukhas. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage



class APIManager{
   
   
    func login(completed:@escaping ([IphoneModel]?)->()){
        Alamofire.request(BASE_URL, method: .get, encoding: URLEncoding.default).responseJSON { (response) in
            if let data = response.data {
                do {
                    let course =  try JSONDecoder().decode(ResponseList.self, from: data)
                    if let list = course.list {
                      completed(list)
                    }
                }catch {
                print(error)
                }
                completed(nil)
          }
    }
}
}
